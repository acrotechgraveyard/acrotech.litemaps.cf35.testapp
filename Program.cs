﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Acrotech.LiteMaps.Engine;
using Acrotech.PortableLogAdapter.Managers;

namespace Acrotech.LiteMaps.CF35.TestApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            Services.Initialize(DebugLogManager.Default);

            Application.Run(new MainWindow());
        }
    }
}