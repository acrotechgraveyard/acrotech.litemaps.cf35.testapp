﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.WinForms.Layers;
using System.Windows.Input;
using Resco.UIElements;
using Acrotech.LiteMaps.Engine.Sources;

namespace Acrotech.LiteMaps.CF35.TestApp
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();

            Initialize();

            Disposed += (s, e) => { Gps.Dispose(); };
        }

        private MapViewModel ViewModel { get; set; }
        private Acrotech.LiteMaps.WinForms.GpsSimulator Gps { get; set; }

        private void Initialize()
        {
            Map.ViewModel = ViewModel = new MapViewModel(
                new GoogleTileImageSource(
                    TileImageSource.GetTileStorage(), 
                    GoogleTileType.Satellite, 
                    GoogleTileImageSource.DefaultSatelliteVersion, 
                    GoogleTileImageSource.DefaultLanguage, 
                    GoogleTileImageSource.DefaultMinZoom, 
                    18
                )
            );

            BindCommand(ViewModel.ZoomIn, ZoomInButton);
            BindCommand(ViewModel.ZoomOut, ZoomOutButton);
            BindCommand(ViewModel.CenterOnGps, CenterOnGPSButton);
            BindCommand(ViewModel.CenterOnTarget, CenterOnTargetButton);

            ViewModel.PropertyChanged += (s, e) => { if (e.PropertyName == "GpsPosition") RunOnUIThread(() => DisplayText.Text = ViewModel.GpsPosition.ToString("d.dddddd")); };

            Map.AddLayer(new FPSLayer(Map.ViewPort, LayerCornerPosition.TopLeft));
            Map.AddLayer(new TileDownloadStatusLayer(Map.ViewPort, LayerCornerPosition.BottomLeft));
            Map.AddLayer(new TargetMarkerLayer());
            Map.AddLayer(new GpsMarkerLayer());

            Gps = new Acrotech.LiteMaps.WinForms.GpsSimulator();
            ViewModel.TargetPosition = Gps.CurrentPosition;
            ViewModel.TargetAccuracy = Gps.CurrentAccuracy;
            Gps.PositionChanged += (p, a) => { ViewModel.GpsPosition = p; ViewModel.GpsAccuracy = a; };
            Gps.Start();
        }

        private void BindCommand(ICommand command, UIButton button)
        {
            command.CanExecuteChanged += (s, e) => RunOnUIThread(() => button.Enabled = command.CanExecute(null));
            button.Click += (s, e) => command.Execute(null);

            button.Enabled = command.CanExecute(null);
        }

        private void RunOnUIThread(Action action)
        {
            if (InvokeRequired)
            {
                try
                {
                    Invoke(action);
                }
                catch (ObjectDisposedException)
                {
                }
            }
            else
            {
                action();
            }
        }
    }
}